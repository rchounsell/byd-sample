import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { DateRegistry } from 'src/app/model /date-registry.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class DateRegistryService extends AbstractService<DateRegistry>{

  constructor(http: HttpClient) {
    super(http, rest.DATE_REGISTRY)
  }
}
