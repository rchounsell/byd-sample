import { TestBed } from '@angular/core/testing';

import { DateRegistryService } from './date-registry.service';

describe('DateRegistryService', () => {
  let service: DateRegistryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DateRegistryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
