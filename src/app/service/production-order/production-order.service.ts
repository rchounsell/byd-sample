import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { ProductionOrder } from 'src/app/model /production-order.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class ProductionOrderService extends AbstractService<ProductionOrder>{

  constructor(http: HttpClient) {
    super(http, rest.PRODUCTION_ORDER)
  }
}
