import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AbstractModel } from "src/app/model /abstract/model.abstract";

export abstract class AbstractService <T extends AbstractModel>{
    private url: string =  "";
    constructor(private http: HttpClient, ENTITY_URL: string){
       this.url = ENTITY_URL;
    }


//READ
    get(): Observable<any>{
        return this.http.get(this.url);
    }
 // UPDATE
    update(data: T): Observable<any> {
        return this.http.put(this.url + "update", data);
    }
//CREATE
    save(data: T): Observable <any>{
        return this.http.post(this.url + "save", data);
    }
    //DELETE 
    delete () {

    }
}