import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { Employee } from 'src/app/model /employee.model';
import { Process } from 'src/app/model /process.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class ProcessService extends AbstractService<Process>{

  constructor(http: HttpClient) {
    super(http, rest.PROCESS)
  }
}
