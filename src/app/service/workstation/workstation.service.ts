import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { Workstation } from 'src/app/model /workstation.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class WorkstationService extends AbstractService<Workstation>{

  constructor(http: HttpClient) {
    super(http, rest.WORKSTATION)
  }
}
