import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { Employee } from 'src/app/model /employee.model';
import { Module } from 'src/app/model /module.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class ModuleService extends AbstractService<Module>{

  constructor(http: HttpClient) {
    super(http, rest.MODULE)
  }
}
