import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { Employee } from 'src/app/model /employee.model';
import { ExtraField } from 'src/app/model /extra-field.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class ExtraFieldService extends AbstractService<ExtraField>{

  constructor(http: HttpClient) {
    super(http, rest.EXTRA_FIELD)
  }
}
