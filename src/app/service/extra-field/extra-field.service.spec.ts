import { TestBed } from '@angular/core/testing';

import { ExtraFieldService } from './extra-field.service';

describe('ExtraFieldService', () => {
  let service: ExtraFieldService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExtraFieldService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
