import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { rest } from 'src/app/constant/rest.constant';
import { Employee } from 'src/app/model /employee.model';
import { ProductionItem } from 'src/app/model /production-item.model';
import { AbstractService } from '../abstract/service.abstract';

@Injectable({
  providedIn: 'root'
})
export class ProductionItemService extends AbstractService<ProductionItem>{

  constructor(http: HttpClient) {
    super(http, rest.PRODUCTION_ITEM)
  }
}
