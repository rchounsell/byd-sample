import { TestBed } from '@angular/core/testing';

import { ProductionItemService } from './production-item.service';

describe('ProductionItemService', () => {
  let service: ProductionItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductionItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
