import { ExtraField } from "./extra-field.model";
import { Process } from "./process.model";
import { Employee } from "./employee.model";
import { AbstractModel } from "./abstract/model.abstract";

export class Workstation extends AbstractModel{
    description: string =  "";
    employee: Employee;
    processes: Array<Process> = new Array();
    extraFields: Array<ExtraField> = new Array();
}