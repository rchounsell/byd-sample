import { AbstractModel } from "./abstract/model.abstract";

export class ExtraField  extends AbstractModel{
    description: string = "";
    value: string = "";
}