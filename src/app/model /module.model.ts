import { AbstractModel } from "./abstract/model.abstract";

export class Module extends AbstractModel{
    description: string = "";
    cell: number = 0;
}