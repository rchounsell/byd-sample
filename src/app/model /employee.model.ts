import { AbstractModel } from "./abstract/model.abstract";

export class Employee extends AbstractModel {
    name: string = "";
    code: string = "";
}