import { AbstractModel } from "./abstract/model.abstract";
import { Process } from "./process.model";

export class DateRegistry extends AbstractModel  {
    initDate: Date = new Date();
    endDate: Date =  new Date() ;
    process: Process  = new Process();
}