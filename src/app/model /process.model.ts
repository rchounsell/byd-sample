import { AbstractModel } from "./abstract/model.abstract";
import { DateRegistry } from "./date-registry.model";
import { Employee } from "./employee.model";

export class Process  extends AbstractModel{
    description: string = "";
    operator: Employee =  new Employee();
    dataRegistry: DateRegistry = new DateRegistry();
}