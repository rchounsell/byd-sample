import { AbstractModel } from "./abstract/model.abstract";

export class ProductionItem  extends AbstractModel {
    description: string = "";
    quantity: number =  0;
}