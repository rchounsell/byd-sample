import { ProductionItem } from "./production-item.model";
import { AbstractModel } from "./abstract/model.abstract";

export class ProductionOrder  extends AbstractModel{
    poNumber: number = 0;
    productionItem: Array<ProductionItem> = new  Array();
}