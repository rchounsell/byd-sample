import { environment } from "src/environments/environment";

export const rest = {
    BASE_URL: environment.base_url,
    DATE_REGISTRY:'/date-registry',
    EMPLOYEE: '/employee',
    EXTRA_FIELD: '/extra-field',
    PROCESS: '/process',
    MODULE: '/module',
    PRODUCTION_ORDER: '/production-order',
    PRODUCTION_ITEM: '/production-item',
    WORKSTATION: '/workstation'
  };